﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConversorDeBase64ParaImagem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                string texto = System.IO.File.ReadAllText(file);

                //salva o text base64 da imagem
                byte[] array = Convert.FromBase64String(texto);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(array);
                Bitmap bit = new Bitmap(ms);
                pictureBox1.Image = bit;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string> fileslist = new List<string>();
            
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                ProcessDirectory(folderBrowserDialog1.SelectedPath);
            }
        }

        public static void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory. 
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName);
 
        }

        private static void ProcessFile(string fileName)
        {
            string file = fileName;
            string texto = System.IO.File.ReadAllText(file);

            //salva o text base64 da imagem
            byte[] array = Convert.FromBase64String(texto);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(array);
            Bitmap bit = new Bitmap(ms);
            bit.Save(file + ".jpg");
            Thread.Sleep(100); 
        }

    }
}
